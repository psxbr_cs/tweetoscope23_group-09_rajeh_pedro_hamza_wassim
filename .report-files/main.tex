\documentclass[10pt, a4paper]{article}
\usepackage[top=3cm, bottom=4cm, left=3.5cm, right=3.5cm, headsep=3mm]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb,amscd, fancyhdr, color, comment, graphicx, environ}
\usepackage{float, mathrsfs, lastpage, xcolor, mdframed, enumerate, enumitem, indentfirst, listings, sectsty, thmtools, shadethm, hyperref, setspace}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{float}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{array}
\usepackage{hyphenat}
\PassOptionsToPackage{hyphens}{url}
\usepackage{hyperref}
\usepackage{caption}
% Additional setup (mdframed, theorem, color definitions, etc.)
\mdfsetup{skipabove=\topskip,skipbelow=\topskip}
% ... (Other setup commands)

\hypersetup{linkcolor=black}



\title{Tweetoscope \\ Final Report}
\author{Group 9}
\date{December 2023}

\begin{document}
\maketitle
Referent : Virginie Galtier

\vspace*{110mm}
\maketitle
Students :
\begin{itemize}
    \item BELHAJ Wassim
    \item EL SADDI Rajeh
    \item HARIS Hamza
    \item SACRAMENTO X. B. ROSA Pedro
\end{itemize} 



\newpage


\section{Secrets and Git}
Storing secrets, such as API tokens, in a Git repository is generally not a good practice. Git repositories are typically meant for code and configuration files, and they are often shared among a team or made public. When we store secrets in a Git repository, they become part of the repository's history, and anyone with access to the repository can potentially see those secrets, posing a significant security risk. If collaborators are involved or the repository is public, controlling access to these secrets becomes challenging, and even if secrets are later removed or changed, they may still be present in the repository's history. Secrets like API tokens may need to be revoked and reissued and storing them in a Git repository makes it cumbersome to update all instances, potentially leaving vulnerabilities. Many best practices and industry standards, such as the OWASP Top Ten, recommend against storing secrets in version control systems.

Best practices for managing secrets in Git involve using environment variables, keeping secrets separate from code and configuration files. Employing “.gitignore” helps exclude sensitive files from being added to the repository, preventing accidental inclusion. Securing the repository involves removing or replacing accidentally committed secrets, and Git hooks can be used to validate that secrets are not being committed. In synthesis, storing secrets in a Git repository is not recommended due to security, access control, and compliance concerns. Instead, industry best practices emphasize secure and separate storage methods to reduce the risk of accidental exposure and potential security breaches. 

\section{Architectural Choices}
The architecture we will use in our project is the following :

    \subsection{TweetProducer}
    A TweetProducer service that produces tweets depending on the case (Random tweets, Recorded tweets from TestBases .txt files, or Scenario case) and store them in the topic “tweets” with 2 partitions. It is a stateless service since it generates tweets independently without maintaining an internal state related to the produced tweets. Each tweet is sent as a separate event without reliance on the previous state.

    \subsection{TweetFilter}
    A TweetFilter service that reads from the topic “tweets”. It filters tweets based on filterchoice (languageEn filters English tweets, languageFr filters French tweets , length100 filters tweets based on their size, in our case between 0 and 100) and publishes them to the “tweetsfiltered” topic with one partition. We use 2 filter services in parallel to handle increased load. It is a stateless service since each tweet is processed independently. 

    \subsection{HashtagExtractor}
    A HashtagExtractor service that reads from the topic “tweetsfiltered”. It extracts hashtags from the “tweetsfiltered” topic and publishes them to the “hashtagsextracted” topic, with one partition. It is a stateless service since the extraction logic is independent for each tweet. It processes each tweet individually without requiring context from previous tweets.

    \subsection{HashtagCounter}
    A HashtagCounter service that reads from the “hashtagsextracted” topic. It counts the occurrences of each hashtag, selects the top 5, and publishes them to the “hashtagscounted” topic, with 1 partition. It is a stateful service since it continuously update the hashtag counts in real-time, the service might need to maintain an internal state to keep track of the counts. This state is continuously updated as new tweets arrive, reflecting the current count of hashtags.

    \subsection{Visualizor}
    A Visualizor service that reads from the “hashtagscounted” topic and visualizes the top 5 hashtags on the GUI interface. It is a stateless service since it directly reads the current state of the hashtagscounted topic and updates the visualization

    \subsection{Remarks}
    The filter services, hashtagExtractor service, hashtagCounter service and Visualizor service need to agree on the format of the messages stored in the "tweets" topic. We chose to use a JSON-formatted string.
    Our project is adapted to work directly on the Random case, since it is the closest approach to have a continuous flow of tweets, similar to the twitter API. But, if you are interested in testing it on Recorded Cases, all you need to do is changing the environment variables of the dockerfiles, docker-compose files, and Kubernetes files to meet your choices.

    \textbf{Note}: When we dockerise our application, since docker doesn’t support GIU interface, we replaced the Visualizor service with a Kafka Consumer for the HashtagCounter service. Hence, instead of displaying the counted hashtags in a GIU interface, we display the count of the top 5 hashtags in the terminal.

\section{Risk Analysis}
    \subsection{Initial Risk Analysis}
    In the proposed architecture, the failure of the TweetProducer service would result in a cessation of tweet generation, impacting the continuous flow of data into the application. A failure in the TweetFilter service could lead to erroneous filtering, potentially allowing unwanted or unfiltered tweets to proceed downstream, impacting the quality of data in the "tweetsfiltered" topic. If one of the two parallel instances of the TweetFilter service fails, the overall capacity for tweet filtering may be reduced, potentially causing higher latencies or delays in processing. If the HashtagExtractor service fails, the extraction of hashtags from filtered tweets would be compromised, affecting the accuracy of hashtag-related data stored in the "hashtagsextracted" topic. A failure in the HashtagCounter service, being stateful and responsible for real-time updates, could result in the loss of live hashtag counting, potentially providing outdated or inaccurate top 5 hashtags in the "hashtagscounted" topic. The Visualizor service's failure would impact the display of top hashtags in the GUI interface, leading to a lack of real-time insights for users. Overall, component failures could disrupt the application's functionality, leading to data inconsistency, potential slowdowns in tweet processing, and a compromised user experience.

    \subsection{Risk mitigation using Kubernetes}
    The provided Kubernetes files encompass the orchestration of a distributed tweet processing application. The Zookeeper service and pod establish the foundational coordination for the Kafka broker, facilitating efficient communication. The Kafka service and broker deployment configure Kafka-related settings and establish connections with the Zookeeper service. Deployments for the TweetProducer, TweetFilter (with two replicas for increased load handling), HashtagExtractor, HashtagCounter, and CounterConsumer services are defined, each specifying container images, necessary environment variables, and replication strategies. These components collectively form a fault-tolerant and scalable architecture for processing, filtering, and counting hashtags. The files also reflect Kubernetes best practices, utilizing services, deployments, and pod specifications to enhance manageability, scalability, and resilience within the cluster.

        Fault tolerance is inherently supported by Kubernetes' self-healing mechanisms. For instance, consider the scenario where the counter-deployement pod experiences a failure, either due to a pod deletion or a node failure. In such an event, Kubernetes detects the discrepancy between the desired state, as defined in the deployment, and the actual state of the cluster. Kubernetes automatically initiates the creation of a replacement pod, ensuring that the counter-deployement remains available. This replacement pod may be scheduled on a different node to enhance fault isolation. The service associated with the counter-deployement further contributes to fault tolerance by providing a stable endpoint for accessing Kafka, facilitating seamless redirection to healthy broker instances. Overall, Kubernetes' ability to dynamically respond to failures by creating and scheduling new pods, exemplified in the Kafka broker scenario, underscores the platform's robust fault tolerance and self-healing capabilities.

    If a node is intentionally deleted or experiences a failure in a Kubernetes cluster hosting the tweet processing application, the system's fault-tolerance mechanisms will come into play to maintain the overall health and availability of the application. Let's take the example of the TweetProducer pod. If the node hosting the TweetProducer pod is deleted, Kubernetes will detect the node failure and trigger the creation of a replacement pod for TweetProducer. This replacement pod will be scheduled on another available and healthy node in the cluster, ensuring continuous tweet generation. Additionally, if there is a service associated with the TweetProducer pod, it ensures a stable endpoint for accessing the tweet generation functionality, contributing to fault tolerance by transparently redirecting traffic to the newly scheduled pod. Overall, Kubernetes demonstrates resilience in the face of node deletions, dynamically redistributing pods to maintain the desired state and uphold the reliability of the tweet processing application. For the node deletion, this is only a theoretical approach, since we are still not able to get the nodes after the electric cut from the 2nd of December.

\section{Return of Experience}
Completing this project has enriched our skills across critical aspects of contemporary software development. We gained hands-on experience in containerization using Docker, crafting Dockerfiles, and orchestrating containers with Docker Compose and Kubernetes. Managing images from DockerHub and GitLab Container Registry enhanced our understanding of image distribution and deployment. Setting up a GitLab CI pipeline automated the build, test, and deployment processes, promoting efficient and reliable software delivery. Integrating SonarQube into the CI pipeline underscored the importance of code quality for scalability and security. Deployment on Kubernetes provided insights into fault tolerance and self-healing mechanisms, essential for building resilient distributed systems. Collaboration using GitLab for version control emphasized the significance of teamwork and versioning practices. This project encapsulated the multifaceted aspects of modern software development, preparing us for real-world challenges in the dynamic software engineering landscape.

\section{Conclusion}
Designing an adaptable architecture for the tweet processing application proved to be a significant challenge, particularly in creating a system that seamlessly accommodates diverse use cases such as Random, Recorded, and Scenario scenarios. The complexity arises from the need to build a modular and flexible structure capable of handling different data sources and processing requirements. Ensuring the architecture's adaptability involves making strategic decisions about how components interact, how they scale, and how they maintain state or statelessness. The challenge lies in achieving a delicate balance between modularity, scalability, and maintainability to meet the dynamic nature of the input data and processing scenarios. This demands a deep understanding of the application's functionalities and thoughtful architectural choices to create a robust, versatile system capable of handling a variety of use cases efficiently. Additionally, during the orchestration phase, we faced a challenge with our initial architecture, which was designed to include two Kafka brokers. Unfortunately, we encountered difficulties when attempting to create two pods of the Kafka broker in the zookeeper-kafka.yml file. To overcome this obstacle, we made the decision to adapt the architecture to accommodate only one Kafka broker.

\end{document}
