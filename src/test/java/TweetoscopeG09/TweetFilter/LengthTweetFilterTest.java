package TweetoscopeG09.TweetFilter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.twitter.clientlib.model.Tweet;

class LengthTweetFilterTest {

	@Test
	void test() {
		LengthTweetFilter filter000100 = new LengthTweetFilter(  0, 100);
		LengthTweetFilter filter100200 = new LengthTweetFilter(100, 200);
		LengthTweetFilter filter150100 = new LengthTweetFilter(150, 100);
		
		int[] tweetLens = {0, 75, 100, 125, 200};
		Tweet[] tweets = new Tweet[tweetLens.length];
		
		for(int i = 0; i < tweetLens.length; i++) {
			String text = new String(new char[tweetLens[i]]).replace('\0', ' ');
			tweets[i] = new Tweet();
			tweets[i].setText(text);
		}
		
		assertEquals(filter000100.match(tweets[0]), true );
		assertEquals(filter000100.match(tweets[1]), true );
		assertEquals(filter000100.match(tweets[2]), true );
		assertEquals(filter000100.match(tweets[3]), false );
		assertEquals(filter000100.match(tweets[4]), false );
		
		assertEquals(filter100200.match(tweets[0]), false );
		assertEquals(filter100200.match(tweets[1]), false );
		assertEquals(filter100200.match(tweets[2]), true );
		assertEquals(filter100200.match(tweets[3]), true );
		assertEquals(filter100200.match(tweets[4]), true );
		
		assertEquals(filter150100.match(tweets[0]), false );
		assertEquals(filter150100.match(tweets[1]), false );
		assertEquals(filter150100.match(tweets[2]), false );
		assertEquals(filter150100.match(tweets[3]), false );
		assertEquals(filter150100.match(tweets[4]), false );
	}

}
