package TweetoscopeG09.HashtagCounter;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Consumed;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class HashtagCounterStream {
    private String bootstrapServers;
    private String inputTopicName;
    private String outputTopicName;
    private int nbLeaders;

    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("Usage: HashtagCounterStream <bootstrapServers> <inputTopicName> <outputTopicName> <int nbLeaders>");
            System.exit(1);
        }

        new HashtagCounterStream(args[0], args[1], args[2], args[3]);
    }

    HashtagCounterStream(String bootstrapServers, String inputTopicName, String outputTopicName, String nbLeaders) {
        this.bootstrapServers = bootstrapServers;
        this.inputTopicName = inputTopicName;
        this.outputTopicName = outputTopicName;
        this.nbLeaders = Integer.parseInt(nbLeaders);

        Topology counterTopology = createTopology();
        KafkaStreams counterStream = new KafkaStreams(counterTopology, configureKafkaStreams());
        counterStream.start();
    }

    private Properties configureKafkaStreams() {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "hashtagCounter");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put("auto.offset.reset", "earliest");
        return properties;
    }

    private Topology createTopology() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        KStream<Void, String> counterStream = streamsBuilder.stream(inputTopicName, Consumed.with(Serdes.Void(), Serdes.String()));
        
        // We're using a map to store the counts
        Map<String, Long> hashtagOccurrenceMap = new HashMap<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        // First we update the count on the sorted map and then we return the complete count
        KStream<Void, String> hashtagCountsStream = counterStream
        		.mapValues(hashtag -> {
        			hashtagOccurrenceMap.compute(hashtag, (k, v) -> (v == null) ? 1 : v + 1);
        			Map<String, Long> topHashtagsMap = hashtagOccurrenceMap.entrySet().stream()
        					.sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(nbLeaders)
        					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        			return gson.toJson(topHashtagsMap).toString();
        		});
        hashtagCountsStream.to(outputTopicName, Produced.with(Serdes.Void(), Serdes.String()));

        return streamsBuilder.build();
    }
};
