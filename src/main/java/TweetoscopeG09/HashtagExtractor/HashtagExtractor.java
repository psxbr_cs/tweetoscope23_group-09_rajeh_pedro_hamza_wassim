package TweetoscopeG09.HashtagExtractor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow.Publisher;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import com.twitter.clientlib.model.Tweet;
import com.twitter.twittertext.Extractor;

public class HashtagExtractor implements Subscriber<Tweet>, Publisher<String> {
	/**
	 * List of objects to notify when a hashtag is extracted (downstream component =
	 * HashtagCounter)
	 */
	protected List<Subscriber<? super String>> subscribers;

	/**
	 * Twitter lib utility object
	 */
	final Extractor twitterTextExtractor = new Extractor();

	public HashtagExtractor() {
		subscribers = new ArrayList<Subscriber<? super String>>();
	}

	@Override
	public void subscribe(Subscriber<? super String> subscriber) {
		subscribers.add(subscriber);
	}

	/**
	 * Triggered when the upstream component (TweetFilter) passes a new Tweet via
	 * Java Flow
	 */
	@Override
	public void onNext(Tweet tweet) {
		if (tweet != null) {
			// extracts the hashtags in the Tweet
			List<String> hashtags = twitterTextExtractor.extractHashtags(tweet.getText());

			// passes down the hashtags to the subscribers
			for (String hashtag : hashtags) {
				for (Subscriber<? super String> s : subscribers) {
					s.onNext(hashtag);
				}
			}
		}
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
	}
}
	
