package TweetoscopeG09.HashtagExtractor;

import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.twitter.clientlib.model.Tweet;
import com.twitter.twittertext.Extractor;

import TweetoscopeG09.TweetFilter.TweetDeserializer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import java.time.OffsetDateTime;

public class HashtagExtractorStream {
    private String bootstrapServers;
    private String inputTopicName;
    private String outputTopicName;

    public static void main(String[] arg) {
        new HashtagExtractorStream(arg[0], arg[1], arg[2]);
    }

    HashtagExtractorStream(String bootstrapServers, String inputTopicName, String outputTopicName) {
        this.bootstrapServers = bootstrapServers;
        this.inputTopicName = inputTopicName;
        this.outputTopicName = outputTopicName;

        Topology extractorTopology = createTopology();
        KafkaStreams extractorStream = new KafkaStreams(extractorTopology, configureKafkaStreams());
        extractorStream.start();
    }

    private Properties configureKafkaStreams() {
        Properties properties = new Properties();
        // Use a more descriptive application ID
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "hashtagExtractor");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return properties;
    }

    private Topology createTopology() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        KStream<Void, String> extractorStream = streamsBuilder.stream(inputTopicName);
        KStream<Void, String> hashtagStream = extractorStream.flatMapValues(this::extractHashtags);
        hashtagStream.to(outputTopicName);
        return streamsBuilder.build();
    }

    private List<String> extractHashtags(String jsonString) {
    	Tweet tweet = TweetDeserializer.fromString(jsonString);    
        Extractor twitterTextExtractor = new Extractor();
        return twitterTextExtractor.extractHashtags(tweet.getText());
    }
}
