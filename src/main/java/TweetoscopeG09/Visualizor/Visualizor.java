package TweetoscopeG09.Visualizor;

import java.awt.Color;
import java.awt.Dimension;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Stream;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import com.google.gson.Gson;

public class Visualizor extends JFrame{
	private String bootstrapServers;
    private String inputTopicName;
    private int nbLeaders;
    protected DefaultCategoryDataset dataset;
    protected static final Comparable ROW_KEY = "hashtag";

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage: Visualizor <bootstrapServers> <inputTopicName> <int nbLeaders>");
            System.exit(1);
        }

        new Visualizor(args[0], args[1], args[2]);
    }

    Visualizor(String bootstrapServers, String inputTopicName, String nbLeaders) {
        this.bootstrapServers = bootstrapServers;
        this.inputTopicName = inputTopicName;
        this.nbLeaders = Integer.valueOf(nbLeaders);
        
        createChart();
        visualizeHashtags();
    }
    
    private void createChart() {
    	dataset = new DefaultCategoryDataset();

		JFreeChart chart = ChartFactory.createBarChart("Most Popular Hashtags", // title
				"", // category axis label
				"number of occurences", // value axis label
				dataset, // category dataset
				PlotOrientation.HORIZONTAL, // orientation
				false, // legend
				true, // tooltips
				false); // urls
		chart.getCategoryPlot().setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		chart.getCategoryPlot().setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		chartPanel.setBackground(Color.white);
		chartPanel.setPreferredSize(new Dimension(500, 300));
		this.add(chartPanel);

		this.pack();
		this.setTitle("Tweetoscope");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
    }

    private void visualizeHashtags() {
        try (KafkaConsumer<Void, String> consumer = new KafkaConsumer<>(configureConsumer())) {
            consumer.subscribe(java.util.Collections.singletonList(inputTopicName));

            while (true) {
                ConsumerRecords<Void, String> records = consumer.poll(Duration.ofMillis(100));

                records.forEach(record -> {
                	Map<String, Long> hashtags = Visualizor.jsonToMap(record.value());
                    updateChart(hashtags);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void updateChart(Map<String, Long> item) {
    	SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// clears the leader board from previous data
				dataset.clear();
				// populates the leader board using data from the received map
				// sorted by reverse number of occurrences
				Stream<Entry<String, Long>> sortedTopHashtags = item.entrySet().stream()
						.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));
				sortedTopHashtags.forEach(t -> {
					dataset.setValue(t.getValue(), ROW_KEY, t.getKey().toString());
				});
				// adds padding, if necessary (if we have not yet observed as many hashtags as
				// expected for the leader board
				for (int i = item.entrySet().size(); i < nbLeaders; i++) {
					dataset.setValue(0, ROW_KEY, "");
				}
			}
		});

    }
    
    private Properties configureConsumer() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "visualizor");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest"); // note that this is different from the other packages
        return properties;
    }
    
    static public Map<String, Long> jsonToMap(String jsonString) {
    	try {
    		// Create Gson object
            Gson gson = new Gson();

            // Deserialize JSON string into Map
            Map<String, Long> map = gson.fromJson(jsonString, Map.class);
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
