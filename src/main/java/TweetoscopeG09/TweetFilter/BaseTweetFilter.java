package TweetoscopeG09.TweetFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow.Publisher;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import com.twitter.clientlib.model.Tweet;


public abstract class BaseTweetFilter implements Subscriber<Tweet>, Publisher<Tweet> {

	protected List<Subscriber<? super Tweet>> subscribers;

	public BaseTweetFilter() {
		subscribers = new ArrayList<Subscriber<? super Tweet>>();
	}

	protected abstract boolean match(Tweet tweet);

	@Override
	public void subscribe(Subscriber<? super Tweet> subscriber) {
		subscribers.add(subscriber);
	}

	@Override
	public void onNext(Tweet tweet) {
		if (match(tweet)) {
			// Notify the subscribers of the non-filtered out Tweet
			for (Subscriber<? super Tweet> s : subscribers) {
				s.onNext(tweet);
			}
		}
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
	}
}