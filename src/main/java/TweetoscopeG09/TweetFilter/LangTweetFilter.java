package TweetoscopeG09.TweetFilter;

import com.twitter.clientlib.model.Tweet;

public class LangTweetFilter extends BaseTweetFilter {

	/**
	 * target language to match (examples: "fr", "en"...)
	 */
	protected String language;

	/**
	 * Creates a filter that tests whether the "language" tag of a Tweet (if it is
	 * set) equals a given code.
	 * 
	 * @param language target language to match (example: "en")
	 */
	public LangTweetFilter(String language) {
		this.language = language;
	}

	@Override
	protected boolean match(Tweet tweet) {
		return tweet.getLang().equals(language);
	}

}
