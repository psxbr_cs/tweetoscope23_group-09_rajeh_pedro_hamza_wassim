package TweetoscopeG09.TweetFilter;
import java.time.OffsetDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.twitter.clientlib.model.Tweet;

public class TweetDeserializer {
	public static Tweet fromString(String jsonString) {
	        
	        try {
	            // Parse the modified string as a JSON object
	        	Gson gson = new GsonBuilder()
	                    .registerTypeAdapter(OffsetDateTime.class, (JsonDeserializer<OffsetDateTime>) (json, type,
	                            context) -> null)
	                    .create();
	            Tweet tweet = gson.fromJson(jsonString, Tweet.class);
	            return tweet;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
		}
}
