package TweetoscopeG09.TweetFilter;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.twitter.clientlib.model.Tweet;

import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

public class TweetFilterStream {
	private String bootstrapServers;
	private String inputTopicName;
	private String outputTopicName;
	private BaseTweetFilter filterClass;

	public static void main(String[] arg) {
		new TweetFilterStream(arg[0], arg[1], arg[2], arg[3]);  
	}

	TweetFilterStream(String bootstrapServers, String inputTopicName, String outputTopicName, String filterClass) {
		this.bootstrapServers = bootstrapServers;
		this.inputTopicName = inputTopicName;
		this.outputTopicName = outputTopicName;
		
		switch (filterClass) {
	    case "languageEn":
	        this.filterClass = new LangTweetFilter("en");
	        break;
	    case "languageFr":
	        this.filterClass = new LangTweetFilter("fr");
	        break;
	    case "length100":
	        this.filterClass = new LengthTweetFilter(0, 100);
	        break;
	    default:
	        System.out.println("Invalid filter choice: '" + filterClass + "'. Please select from: [languageEn, languageFr, length100]");
	        break;
	}

		Topology filterTopology = createTopology();
		KafkaStreams filterStream = new KafkaStreams(filterTopology,
				configureKafkaStreams());
		filterStream.start();
	}

	private Properties configureKafkaStreams() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "filteredTweet");
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass().getName());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return properties;
	}

	private Topology createTopology() {
		StreamsBuilder streamsBuilder = new StreamsBuilder();

		KStream<Void, String> filterStream = streamsBuilder.stream(inputTopicName);
		
		Predicate<Void, String> predicateFilter = new Predicate<Void, String>() {
			public boolean test(Void key, String string) {
				Tweet tweet = TweetDeserializer.fromString(string);
				return filterClass.match(tweet);
			}
		};
		
		KStream outputStream = filterStream.filter(predicateFilter);
		
		outputStream.to(outputTopicName);
		
		return streamsBuilder.build();
	}

}

