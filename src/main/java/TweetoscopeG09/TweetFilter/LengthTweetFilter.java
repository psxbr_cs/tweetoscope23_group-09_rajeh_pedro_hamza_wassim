package TweetoscopeG09.TweetFilter;

import com.twitter.clientlib.model.Tweet;

public class LengthTweetFilter extends BaseTweetFilter {

	/**
	 * Filters Tweets according to the message length.
	 *
	 */
		
	private int minLen;
	private int maxLen;
	
	public LengthTweetFilter(int minLen, int maxLen) {
		this.minLen = minLen;
		this.maxLen = maxLen;
	}
	
	@Override
	protected boolean match(Tweet tweet) {
		int tweetLen = tweet.getText().length();
		return (this.minLen <= tweetLen) && (tweetLen <= this.maxLen);
	}

}
