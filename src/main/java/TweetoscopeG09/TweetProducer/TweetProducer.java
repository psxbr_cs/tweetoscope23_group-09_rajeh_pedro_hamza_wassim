package TweetoscopeG09.TweetProducer; 

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Flow.Subscriber;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.twitter.clientlib.model.Tweet;

public class TweetProducer {
    protected List<Subscriber<? super Tweet>> subscribers;
    private KafkaProducer<Void, String> kafkaProducer;
    private String bootstrapServers;
    private String topicName;

    public static void main(String[] args) {
    	String file = null;
    	if(args.length > 3) {
    		file = args[3];
    	}
        new TweetProducer(args[0], args[1], args[2], file);
    }

    TweetProducer(String bootstrapServers, String topicName, String typeChoice, String filePathAndName) {
        subscribers = new ArrayList<Subscriber<? super Tweet>>();
        this.bootstrapServers = bootstrapServers; 
        this.topicName = topicName;
        try {
            if (typeChoice.equals("Random")) {
                handleRandomType();
            } else if (typeChoice.equals("Recorded")) {              
                handleRecordedType(filePathAndName);
            } else if (typeChoice.equals("Scenario")) {
                handleScenarioType();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClosed() throws Exception {
        System.out.println("The stream connection has been closed.");
    }

    public void onComment(String comment) throws Exception {
        System.out.println("A comment line (any line starting with a colon) was received from the stream: " + comment);
    }

    public void onError(Throwable t) {
        System.out.println("An exception occurred on the socket connection: " + t.getMessage());
    }

    public void subscribe(Subscriber<? super Tweet> subscriber) {
        subscribers.add(subscriber);
    }

    private void handleRandomType() {
        Properties producerProperties = configureKafkaProducer();

        try (KafkaProducer<Void, String> kafkaProducer = new KafkaProducer<>(producerProperties)) {
            String[] hashtags = { "fun", "bitCoin", "climate", "crypto", "CS", "Metz", "weather", "summer", "holidays",
                    "health", "running", "sport" };
            String[] languages = { "fr", "en", "ru", "es", "it" };

            Tweet tweet;
            int nb = 0;
            String text;
            Random r = new Random();
            while (true) {
                // Crafts a random Tweet
                nb++;
                tweet = new Tweet();
                text = "Tweet " + nb;
                for (int i = 0; i < (int) (4 * Math.random()); i++) {
                    double d;
                    do {
                        d = r.nextGaussian();
                        d = (int) (hashtags.length / 2 + d * hashtags.length / 2);
                    } while (d < 0 || d > hashtags.length - 1);
                    text += "#" + hashtags[(int) d] + " ";
                }
                tweet.setId("" + nb);
                tweet.setText(text);
                tweet.setLang(languages[(int) (Math.random() * languages.length)]);

                     
                Gson gson = new GsonBuilder()
                        .registerTypeAdapter(OffsetDateTime.class, (JsonDeserializer<OffsetDateTime>) (json, type,
                                context) -> OffsetDateTime.parse(json.getAsString()))
                        .create();
                String json = gson.toJson(tweet);

                // Publishes the Tweet to Kafka   
            	kafkaProducer.send(new ProducerRecord<>(topicName, null, json));

                // Waits for a while
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleRecordedType(String fileName) {
        Properties producerProperties = configureKafkaProducer();

        try (KafkaProducer<Void, String> kafkaProducer = new KafkaProducer<>(producerProperties)) {
            String jsonString = new String(Files.readAllBytes(Paths.get(fileName)));
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(OffsetDateTime.class, (JsonDeserializer<OffsetDateTime>) (json, type,
                            context) -> OffsetDateTime.parse(json.getAsString()))
                    .create();
            JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);
            JsonArray prerecordedtweets = jsonObject.getAsJsonArray("tweets");
            for (JsonElement je : prerecordedtweets) {
                Tweet tweet = gson.fromJson(je, Tweet.class);
                String json = gson.toJson(tweet);

                // Publishes the Tweet to Kafka   
            	kafkaProducer.send(new ProducerRecord<>(topicName, null, json));
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleScenarioType() {
        Properties producerProperties = configureKafkaProducer();

        try (KafkaProducer<Void, String> kafkaProducer = new KafkaProducer<>(producerProperties)) {
            Tweet[] tweets = new Tweet[10];

            tweets[0] = new Tweet();
    		tweets[0].setId("001");
    		tweets[0].setCreatedAt(OffsetDateTime.of(2022, 6, 20, 11, 46, 23, 0, ZoneOffset.UTC));
    		tweets[0].setText("Choisissez un #travail que vous aimez et vous n'aurez pas à travailler un seul jour de votre vie");
    		tweets[0].setAuthorId("31");
    		tweets[0].setConversationId("01");
    		tweets[0].getGeo();
    		tweets[0].setLang("fr");
    		
    		tweets[1] = new Tweet();
    		tweets[1].setId("002");
    		tweets[1].setCreatedAt(OffsetDateTime.of(2022, 6, 20, 11, 48, 20, 0, ZoneOffset.UTC));
    		tweets[1].setText("Si on travaille pour gagner sa vie, pourquoi se tuer au #travail ?");
    		tweets[1].setAuthorId("31");
    		tweets[1].setConversationId("01");
    		tweets[1].getGeo();
    		tweets[1].setLang("fr");
    		
    		tweets[2] = new Tweet();
    		tweets[2].setId("003");
    		tweets[2].setCreatedAt(OffsetDateTime.of(2022, 6, 24, 6, 6, 13, 0, ZoneOffset.UTC));
    		tweets[2].setText("#Failure is not the opposite of #success: it’s part of success.");
    		tweets[2].setAuthorId("32");
    		tweets[2].setConversationId("01");
    		tweets[2].getGeo();
    		tweets[2].setLang("en");

    		tweets[3] = new Tweet();
    		tweets[3].setId("004");
    		tweets[3].setCreatedAt(OffsetDateTime.of(2022, 7, 2, 4, 44, 17, 0, ZoneOffset.UTC));
    		tweets[3].setText("You are not your resume, you are your #work.");
    		tweets[3].setAuthorId("34");
    		tweets[3].setConversationId("01");
    		tweets[3].getGeo();
    		tweets[3].setLang("en");

    		tweets[4] = new Tweet();
    		tweets[4].setId("005");
    		tweets[4].setCreatedAt(OffsetDateTime.of(2022, 8, 20, 10, 49, 23, 0, ZoneOffset.UTC));
    		tweets[4].setText("People who wonder if the glass is half empty or half full miss the point. The glass is refillable.");
    		tweets[4].setAuthorId("35");
    		tweets[4].setConversationId("03");
    		tweets[4].getGeo();
    		tweets[4].setLang("en");

    		tweets[5] = new Tweet();
    		tweets[5].setId("006");
    		tweets[5].setCreatedAt(OffsetDateTime.of(2022, 9, 20, 11, 23, 31, 0, ZoneOffset.UTC));
    		tweets[5].setText("If you think you are too small to make a difference, try sleeping with a mosquito.");
    		tweets[5].setAuthorId("33");
    		tweets[5].setConversationId("04");
    		tweets[5].getGeo();
    		tweets[5].setLang("en");

    		tweets[6] = new Tweet();
    		tweets[6].setId("007");
    		tweets[6].setCreatedAt(OffsetDateTime.of(2022, 11, 30, 2, 15, 0, 0, ZoneOffset.UTC));
    		tweets[6].setText("Nothing will #work unless you do.");
    		tweets[6].setAuthorId("34");
    		tweets[6].setConversationId("03");
    		tweets[6].getGeo();
    		tweets[6].setLang("en");

    		tweets[7] = new Tweet();
    		tweets[7].setId("008");
    		tweets[7].setCreatedAt(OffsetDateTime.of(2022, 12, 1, 8, 30, 20, 0, ZoneOffset.UTC));
    		tweets[7].setText("If you get tired, learn to rest, not to quit.");
    		tweets[7].setAuthorId("35");
    		tweets[7].setConversationId("05");
    		tweets[7].getGeo();
    		tweets[7].setLang("en");

    		tweets[8] = new Tweet();
    		tweets[8].setId("009");
    		tweets[8].setCreatedAt(OffsetDateTime.of(2023, 1, 2, 7, 55, 56, 0, ZoneOffset.UTC));
    		tweets[8].setText("#Failure is #success in progress.");
    		tweets[8].setAuthorId("33");
    		tweets[8].setConversationId("02");
    		tweets[8].getGeo();
    		tweets[8].setLang("en");

    		tweets[9] = new Tweet();
    		tweets[9].setId("010");
    		tweets[9].setCreatedAt(OffsetDateTime.of(2023, 8, 3, 12, 40, 25, 0, ZoneOffset.UTC));
    		tweets[9].setText("The only place #success comes before #work is in the dictionary.");
    		tweets[9].setAuthorId("36");
    		tweets[9].setConversationId("07");
    		tweets[9].getGeo();
    		tweets[9].setLang("en");

    		Gson gson = new GsonBuilder()
                    .registerTypeAdapter(OffsetDateTime.class, (JsonDeserializer<OffsetDateTime>) (json, type,
                            context) -> OffsetDateTime.parse(json.getAsString()))
                    .create();
            for (int i = 0; i < tweets.length; i++) {
                // Publishes the Tweet to Kafka
            	if(tweets[i] == null) { continue; }
                String json = gson.toJson(tweets[i]);

                // Publishes the Tweet to Kafka   
            	kafkaProducer.send(new ProducerRecord<>(topicName, null, json));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Properties configureKafkaProducer() {
        Properties producerProperties = new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.VoidSerializer");
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        return producerProperties;
    }
}
